import React, { Component, useState } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import _ from 'lodash';
import './index.css';

const App = (props) => {
  return (
    <>
      <h2>Basic Components</h2>
      <Hello name="Hugo" />
      <Counter value={0} />
      <h2>Data Fetching Component</h2>
      <UserApp title="GitHub User List" />
      <h2>Star Match Game</h2>
      <StarMatch />
    </>
  );
};

// TODO: Add components in separate files.

/* Hello *******************/
const Hello = (props) => {
  return <p>Hello, {props.name}!</p>;
};

/* Counter *****************/
const CounterDisplay = (props) => {
  return <h3>Counter: {props.value}</h3>;
};
const CounterButton = (props) => {
  return <button onClick={props.counterFunction}>{props.value}</button>;
};
const Counter = (props) => {
  const [counter, setCounter] = useState(props.value || 0);
  const createIncreaser = (increaseBy) => {
    return () => setCounter(counter + increaseBy);
  };
  return (
    <>
      <CounterDisplay value={counter} />
      <CounterButton counterFunction={createIncreaser(1)} value="+1" />
      <CounterButton counterFunction={createIncreaser(5)} value="+5" />
      <CounterButton counterFunction={createIncreaser(10)} value="+10" />
      <CounterButton counterFunction={createIncreaser(100)} value="+100" />
    </>
  );
};

/* UserApp ***************/
class UserApp extends Component {
  state = {
    users: []
  };
  render() {
    const addUser = u => this.setState({ users: this.state.users.concat([u]) });
    return (
      <>
        <div className="header">{this.props.title}</div>
        <UserSearchForm addUser={addUser} />
        <UserList users={this.state.users} />
      </>
    );
  }
}

class UserSearchForm extends Component {
  state = {
    username: ''
  };
  handleSubmit = async (event) => {
    event.preventDefault();
    const username = this.state.username;
    console.log(`Fetching user with username: ${username}`);
    try {
      const response = await axios.get(`https://api.github.com/users/${username}`);
      console.log('Received response:', response);
      const user = _.pick(response.data, ['id', 'name', 'avatar_url', 'company']);
      console.log('Adding user:', user);
      this.props.addUser(user);
      this.setState({ username: '' });
    } catch (error) {
      console.log(error);
    }
  };
  render() {
    const handleUserNameChange = event => {
      this.setState({ username: event.target.value });
    };
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text" placeholder="GitHub username" required
               value={this.state.username}
               onChange={handleUserNameChange} />
        <button>Add user</button>
      </form>
    );
  }
}

class UserList extends Component {
  render() {
    return (
      <>
        {this.props.users.map(u => <UserCard {...u} key={u.id} />)}
      </>
    );
  }
}

class UserCard extends Component {
  render() {
    const { avatar_url, name, company } = this.props;
    return (
      <div className="github-profile">
        <img src={avatar_url} alt="Avatar" />
        <div className="info">
          <div className="name">
            {name}
          </div>
          <div className="company">
            {company}
          </div>
        </div>
      </div>
    );
  }
}

/* StarMatch ***************/
// TODO:
// - Add Reset button.
// - Add "You Won" / "Game Over" with "Play Again" button.
const StarMatch = () => {
  const initialTargetSum = utils.random(1, 9);
  const initialNumbers = utils.range(1, 9);
  const initialNumbersList = new PlayNumbersList(initialNumbers, initialTargetSum);
  const [numbersList, setNumbersList] = useState(initialNumbersList);

  const onNumberClick = clickedNumber => {
    if (clickedNumber.status === 'used') {
      return;
    }

    setNumbersList((prevList) => {
      const newList = new PlayNumbersList(prevList.numbers, prevList.targetSum);
      let match = false;

      switch(clickedNumber.status) {
        case 'available':
          if (newList.isWrong()) {
            clickedNumber.setWrong();
          } else if (newList.sumWillBeWrong(clickedNumber.value)) {
            clickedNumber.setWrong();
            newList.candidates().setWrong();
          } else if (newList.sumWillBeCandidate(clickedNumber.value)) {
            clickedNumber.setCandidate();
          } else if (newList.sumWillMatch(clickedNumber.value)) {
            clickedNumber.setUsed();
            newList.candidates().setUsed();
            match = true;
          }
          break;
        case 'candidate':
          clickedNumber.setAvailable();
          break;
        case 'wrong':
          if (newList.sumWillBeWrong(-clickedNumber.value)) {
            clickedNumber.setAvailable();
          } else if (newList.sumWillBeCandidate(-clickedNumber.value)) {
            clickedNumber.setAvailable();
            newList.wrong().setCandidate();
          } else if (newList.sumWillMatch(-clickedNumber.value)) {
            clickedNumber.setAvailable();
            newList.wrong().setUsed();
            match = true;
          }
          break;
        default:
          throw RangeError(`Invalid status: ${clickedNumber.status}`)
      }

      if (match) {
        const availableValues = newList.available().values();
        newList.targetSum = utils.randomSumIn(availableValues, availableValues.reverse()[0]);
      }

      return newList;
    });
  };

  return (
    <div className="game">
      <div className="help">
        Pick 1 or more numbers that sum to the number of stars
      </div>
      <div className="body">
        <div className="left">
          <StarsDisplay count={numbersList.targetSum} />
        </div>
        <div className="right">
          <NumberPad numbers={numbersList.numbers} onNumberClick={onNumberClick} />
        </div>
      </div>
      <div className="timer">Time Remaining: 10</div>
    </div>
  );
};

const StarsDisplay = props => {
  const starIds = utils.range(1, props.count);

  return (
    <>
      {
        starIds.map(id =>
          <div key={id} className="star" />
        )
      }
    </>
  )
};

function NumberPad({ numbers, onNumberClick }) {
  return (
    <>
      {
        numbers.map(n =>
          <button key={n.value} className="number" style={{backgroundColor: colors[n.status]}} onClick={() => onNumberClick(n)}>
            {n.value}
          </button>
        )
      }
    </>
  );
}

// Helper class to group numbers and operate on number groups.
class PlayNumbersList {
  constructor(numbers, targetSum) {
    this.numbers = typeof numbers[0] === 'object'
      ? numbers//.sort((a, b) => a.value - b.value),
      : numbers.map(value => new PlayNumber(value));
    this.targetSum = targetSum;
  }
  values() { return this.numbers.map(n => n.value); }
  available() { return new PlayNumbersList(this.numbers.filter(n => n.isAvailable()), this.targetSum); }
  used() { return new PlayNumbersList(this.numbers.filter(n => n.isUsed()), this.targetSum); }
  candidates() { return new PlayNumbersList(this.numbers.filter(n => n.isCandidate()), this.targetSum); }
  candidatesSum() { return utils.sum(this.candidates().values()); }
  wrong() { return new PlayNumbersList(this.numbers.filter(n => n.isWrong()), this.targetSum); }
  wrongSum() { return utils.sum(this.wrong().values()); }
  isWrong() { return this.wrong().numbers.length > 0; }
  currentSum() { return this.candidatesSum() || this.wrongSum(); }
  sumWillBeWrong(clickedValue) { return (this.currentSum() + clickedValue) > this.targetSum; }
  sumWillBeCandidate(clickedValue) { return (this.currentSum() + clickedValue) < this.targetSum; }
  sumWillMatch(clickedValue) { return (this.currentSum() + clickedValue) === this.targetSum; }
  setWrong() { this.numbers.forEach(n => n.setWrong()); }
  setCandidate() { this.numbers.forEach(n => n.setCandidate()); }
  setUsed() { this.numbers.forEach(n => n.setUsed()); }
}

// Helper class to manage number state.
class PlayNumber {
  constructor(value, status = 'available') {
    this.value = value;
    this.status = status;
  }
  isAvailable() { return this.status === 'available'; }
  setAvailable() { this.status = 'available'; }
  isUsed() { return this.status === 'used'; }
  setUsed() { this.status = 'used'; }
  isCandidate() { return this.status === 'candidate'; }
  setCandidate() { this.status = 'candidate'; }
  isWrong() { return this.status === 'wrong'; }
  setWrong() { this.status = 'wrong'; }
}

// Color Theme
const colors = {
  available: 'lightgray',
  used: 'lightgreen',
  wrong: 'lightcoral',
  candidate: 'deepskyblue',
};

// Math science
const utils = {
  // Sum an array
  sum: arr => arr.reduce((acc, curr) => acc + curr, 0),

  // create an array of numbers between min and max (edges included)
  range: (min, max) => Array.from({ length: max - min + 1 }, (_, i) => min + i),

  // pick a random number between min and max (edges included)
  random: (min, max) => min + Math.floor(Math.random() * (max - min + 1)),

  // Given an array of numbers and a max...
  // Pick a random sum (< max) from the set of all available sums in arr
  randomSumIn: (arr, max) => {
    const sets = [[]];
    const sums = [];
    for (let i = 0; i < arr.length; i++) {
      for (let j = 0, len = sets.length; j < len; j++) {
        const candidateSet = sets[j].concat(arr[i]);
        const candidateSum = utils.sum(candidateSet);
        if (candidateSum <= max) {
          sets.push(candidateSet);
          sums.push(candidateSum);
        }
      }
    }
    return sums[utils.random(0, sums.length - 1)];
  },
};

ReactDOM.render(<App />, document.getElementById('root'));
